<?php
/**
    Plugin Name: Dax Wordpress Cleanup.
    Plugin URI:
    Author: Dax Castellon Meyrat
    Author URI: http://daxcastellon.com
    Description: Limpia la cabecera de Wordpress.
    Version: 1.0.0
*/
// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// custom excerpt ellipses for 2.9
function replace_excerpt($content) {
	return str_replace('[...]','...',$content);
}
add_filter('the_excerpt', 'replace_excerpt');

// Remueve el <div> que envuelve la navegación dinámica, para limpiar el código
function remover_div_nav($args = '')
{
    $args['container'] = false;
    return $args;
}
add_filter('wp_nav_menu_args', 'remover_div_nav');

// Remueve las clases, ID's y el ID de páginas del menú de navegación de los <li>.
function remover_ids_clases_nav($var)
{
    return is_array($var) ? array() : '';
}
add_filter('nav_menu_css_class', 'remover_ids_clases_nav', 100, 1); // Remueve las clases en los <li> de navegación
add_filter('nav_menu_item_id', 'remover_ids_clases_nav', 100, 1); // Remueve los ID en los <li> de navegación
add_filter('page_css_class', 'remover_ids_clases_nav', 100, 1); // Remueve los ID de páginas en los <li> de navegación

// Remueve los estilos de comentarios recientes en el wp_head()
function remover_comentarios_recientes()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}
add_action('widgets_init', 'remover_comentarios_recientes');

// Limpieza del <head>. Generalmente esto se hace para tener menos "basura" en el head, por ejemplo, si no se quiere utilizar un RSS y como medidas de seguridad, para evitar que los robots detecten que el sitio está hecho bajo Wordpress
remove_action('wp_head', 'feed_links_extra', 3); // Remueve enlaces a extra feeds en RSS.
remove_action('wp_head', 'feed_links', 2); // Remueve los enlaces a los feeds en general.
remove_action('wp_head', 'rsd_link'); // Remueve los enlaces de RSD.
remove_action('wp_head', 'wlwmanifest_link'); // Remueve el enlace de Windows Live Writer
remove_action('wp_head', 'index_rel_link'); // Remueve el enlace a Index
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Remueve en el enlace Prev
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Remueve el enlace Start
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Remueve los enlaces a los posts adyacentes al post actual
remove_action('wp_head', 'wp_generator'); // Remueve el generador de XHTML y la versiónd e Wordpress
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); // Emoji detection script.
remove_action( 'wp_print_styles', 'print_emoji_styles' ); // Emoji styles.

// Remueve la version de los scripts.
function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
        return $parts[0];
}
add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );